# Expanding SDO Capabilities

This is an exported Git repo for the autocalibration work identified in the paper <fill out paper>

The goal of this work:
Using spatial information to calibrate the extreme UV (EUV) telescopes: EUV telescopes operating in space are known to degrade over the course of months to years. The rate of degradation is, a priori, unknown. Over the same time scales, the Sun's activity also changes. This project aims to use the spatial patterns of features on the Sun to arrive at a self-calibration of EUV instruments. This would avoid the need to calibrate against other sources.

We use the data set described in Galvez et al. (2019, ApJS): https://iopscience.iop.org/article/10.3847/1538-4365/ab1005

# How to use the repo

1) Notebooks live in the notebooks folder and they follow the following naming convention {number}{initial of your name}_{topic}(i.e. 01v_exploration.ipynb)

2) Reusable code lives inside src in the form of a package called sdo that can be installed. This makes easy to import our codes between notebooks and scripts. 
    In order to install the package:
        1) cd autocal-export
        2) pip install --user -e .
    In order to import a specific function inside a notebook just use:
        1) from sdo.name_of_the_module import name_of_the_function
        2) Look at notebooks/01v_explore.ipynb for an example

# Training/Testing Runs

Before you can run the training/testing pipeline, you need to ensure you have your Anaconda and Python PIP environments correctly set up. We use Python 2.7. Make sure Anaconda is installed and that you install required pip packages:

```
pip install -r requirements.txt
```

Now you can run the pipeline. Arguments can be passed to `./src/sdo/main.py` either from the command-line as switches, or as a YAML configuration file. run `./src/sdo/main.py --help` to see a list of available configuration options.

To start a new training run:

```
cd ~/autocal-export
export CONFIG_FILE=config/autocalibration_default.yaml
export EXPERIMENT_NAME=01b_experiment_test
export NUM_EPOCHS=5
./src/sdo/main.py \
    -c $CONFIG_FILE \
    --experiment-name=$EXPERIMENT_NAME \
    --num-epochs=$NUM_EPOCHS
```

Where `CONFIG_FILE` is a path to a YAML file that might have common configuration options
that you don't want to have to type every time on the command line (see the above
`config/autocalibration_default.yaml` for an example); `EXPERIMENT_NAME` is a unique
experiment name used to partition your training results; and NUM_EPOCHS is the total number of training epochs you want.

To resume a previously checkpointed training session:

```
cd ~/autocal-export
export CONFIG_FILE=config/autocalibration_default.yaml
export RESULTS_PATH=/fdl_sw/experiments_results
export START_EPOCH_AT=2
export NUM_EPOCHS=5
./src/sdo/main.py \
    -c $CONFIG_FILE \
    --experiment-name=$EXPERIMENT_NAME \
    --num-epochs=$NUM_EPOCHS \
    --continue-training=True \
    --saved-model-path=$RESULTS_PATH/$EXPERIMENT_NAME/model_epoch_$START_EPOCH_AT.pth \
    --saved-optimizer-path=$RESULTS_PATH/$EXPERIMENT_NAME/optimizer_epoch_$START_EPOCH_AT.pth \
    --start-epoch-at=$START_EPOCH_AT
```

Where `START_EPOCH_AT` is the new training epoch to begin training from.